import os
import tempfile

import pytest
import flask
from hangman import create_app
from hangman.db import get_db, init_db


@pytest.fixture
def app():
    db_fd, db_path = tempfile.mkstemp()

    app = create_app({
        'TESTING': True,
        'DATABASE': db_path,
    })

    with app.app_context():
        init_db()

    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture
def client(app):
    return app.test_client()


def test_new_game(client):
    assert client.get('/game').status_code == 200

    with client:
        rv = client.get('/game')
        assert 'secret_word' in flask.session
        assert 'current_progress' in flask.session
        assert 'wrong_guesses_left' in flask.session

        len_word = len(flask.session['secret_word'])
        assert len(flask.session['current_progress']) == len_word
        assert flask.session['wrong_guesses_left'] == 5
