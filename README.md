# Hangman

This is a small demo project demonstrating a simplified version of the game of Hangman. The application is built using Flask, which serves the endpoints. They return HTML which can be viewed in the browser.

## Installing the game
The game is built using Python. It can be installed using the following commands:
```
python3 -m venv venv
source venv/bin/activate
pip install --editable .
export FLASK_APP=hangman
flask init-db
```

## Running the game
After installation and database initialization, the game can be run using the following command:
```
flask run
```
The interface of the game can then be accessed in the browser at `http://127.0.0.1:5000/`

## Instructions
The game is played by asked one letter at a time, in an attempt to guess the secret word. You are only allowed to have 5 wrong guesses.

## Code structure
The app creation can be found in `hangman/__init__.py`. All game logic can be found in `hangman/hangman.py`. The game has 3 pages:
1. Landing page, where the highscores are listed. A button is show with which the user can start the game
2. The game page. Here, the actual game happens. Using a `GET` request, the client gets a new empty session. The client can then `POST` letters which the user would like to get checked. That request return a new view. When the game is over, the user is redirected to the results page.
3. The results page. Show the user how well they've done and offer a spot in the highscore list if they've succesfully completed the game.
