from flask import (
    Blueprint, Flask, flash, g, redirect, render_template, session, url_for,
    escape, request
)

import sqlite3
import random

from hangman.db import get_db

bp = Blueprint('hangman', __name__)


def random_word() -> str:
    available_words = [
        '3dhubs',
        'marvin',
        'print',
        'filament',
        'order',
        'layer',
    ]

    chosen_word = random.choice(available_words)

    return chosen_word


def update_game(session, chosen_letter):
    # check whether the asked letter occurs in the secret word, save the indices
    indices = [idx for idx in range(len(session['secret_word']))
               if session['secret_word'][idx] == chosen_letter]

    if len(indices) == 0:
        session['wrong_guesses_left'] -= 1
        return

    for idx in indices:
        # replace the '_'s with the (correctly) asked letter
        session['current_progress'][idx] = chosen_letter
        session.modified = True


def compute_score(session):
    session['score'] = session['wrong_guesses_left']


def check_state(session):
    state = 'CONTINUE'
    if '_' not in session['current_progress']:
        state = 'GAME_WON'
        compute_score(session)
    elif session['wrong_guesses_left'] <= 0:
        state = 'GAME_LOST'

    return state


@bp.route('/')
def index():
    # Get the 10 best scores to show on the landing page
    db = get_db()
    highscores = db.execute(
        'SELECT username, score, created'
        ' FROM highscore'
        ' ORDER BY score DESC'
        ' LIMIT 10'
    ).fetchall()

    return render_template('hangman/index.html', highscores=highscores)


@bp.route('/game', methods=['GET', 'POST'])
def game():
    """
    The game page. On GET, create a new session. Subsequent POSTs update that session.
    """
    if request.method == 'POST':
        update_game(session, request.form['letter'])
        state = check_state(session)
        if state == 'GAME_WON' or state == 'GAME_LOST':
            session['state'] = state
            return redirect(url_for('result'))
    else:
        word = random_word()
        session['wrong_guesses_left'] = 5
        session['secret_word'] = word
        session['current_progress'] = list(len(word) * '_')

    return render_template('hangman/game.html')


@bp.route('/result', methods=['GET', 'POST'])
def result():
    """
    Show final score to user. I they had guesses left, let them fill in name.
    The name and score can then be saved to the database.
    """
    if request.method == 'POST':
        username = request.form['username']
        db = get_db()
        db.execute(
            'INSERT INTO highscore (username, score)'
            ' VALUES (?, ?)',
            (username, session['score'])
        )
        db.commit()
        return redirect(url_for('index'))

    return render_template('hangman/result.html')
